hello-world:
	mkdir -p bin
	gcc `pkg-config --cflags gtk+-3.0` -o bin/hello-world examples/hello-world.c `pkg-config --libs gtk+-3.0`

grid:
	mkdir -p bin
	gcc `pkg-config --cflags gtk+-3.0` -o bin/grid examples/grid.c `pkg-config --libs gtk+-3.0`

ui-builder:
	mkdir -p bin
	gcc `pkg-config --cflags gtk+-3.0` -o bin/ui-builder examples/ui-builder.c `pkg-config --libs gtk+-3.0`
	cp examples/builder.ui bin/builder.ui
